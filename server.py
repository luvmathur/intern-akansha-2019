import socket
import select
import config_server

host = config_server.host
port = config_server.port
bytes = config_server.bytes

socket_list = []                     # to keep the address of the clients who have joined the chat box


server = socket.socket(socket.AF_INET , socket.SOCK_STREAM)              # creating a socket
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)                # to close the port

def socketConnection() :

    bindingSocket()

    listeningSignals()

    socket_list.append(server)         # add server to the socket_list

    print("welcome to chat application !\n")

    print("Server working on port " + str(port))      # display's the port you've working on

    while True:

        read ,write , err = select.select(socket_list , [] , [], 0)       # to monitor's any inout output signal

        for socket_ in read:     # if ready_to_read has some

                if socket_ == server :            # accepting a new connection

                    connection , address = server.accept()
                    socket_list.append(connection)       # add a new connection to the socket_list

                    print("Client connected to the chat : " , address)

                    broadcastMsg(server , socket_ ,  "\n\nClient {} is a part of chat room ".format(address))     # send it to all the clients

                else:
                    try:
                        clientMsg = socket_.recv(config_server.bytes)               # Server's reply stored in bytes form

                        if len(str.encode(clientMsg)) :

                            broadcastMsg(server , socket_ , "\n\n" + str(socket_.getpeername()) + ":" + clientMsg)       #returns the port number of the client which has sent the msg

                        else:
                            if socket_ in socket_list:
                                 socket_list.remove(socket_)

                            broadcastMsg(server , socket_ ,"\n\nClient {} left in chat box ".format(address))

                    except:                                                   # to handle the exceptions in case something goes wrong or the clients shuts it's window

                        broadcastMsg(server ,socket_ , "\n\nClients {} left the chat nox".format(address))
                        continue


    server.close()



def sendMsg():           # if server wants to take part in the chat. We are not calling it anywhere though.

    Server_msg = input("\nServer :")

    if str(Server_msg) == "quit":  # If you want to end the conversation
        server.close()
        sys.exit()

    elif len(str.encode(Server_msg)) > 0:  # If length of message sent is greater then zero ,then send it to the client
        broadcastMsg(server, None, Server_msg)

    elif len(str.encode(Server_msg)) == 0:
        print("pls type....")
        Server_msg = input("\nServer :")
        self.connection.send(str.encode(Server_msg))
        print("message sent\n")




def bindingSocket():
    try:
        server.bind((host, port))
        print(config_server.success_msg)

        # Exracting the host server's name
        host_name = socket.gethostname()                             # Exracting the host server's name
        print("THE NAME OF OUR HOST SERVER IS : ", host_name)

    except socket.error as msg:                                      # exception handle block where Binding_Socket is called again to bind the socket.
        print(config_server.failure_msg + str(msg) + "Retrying ")
        bindingSocket()




def listeningSignals():

    try:

        server.listen(config_server.No_Of_failed_signals)
        print(config_server.success_msg)

    except socket.error as msg:                                      # If there is a failure in listening to the signal then exception is shown as a message

        print(config_server.failure_msg + str(msg) + "Retrying ")
        listeningSignals()  # Again trying to listen to the signals





def broadcastMsg(server , socket_ , message ):

    for socketNew in socket_list:

            if socketNew!=server and socketNew!= socket_:     # don't sent it to the server and the client which has sent the data

                try :

                    socketNew.send(message)

                except:                    #connection lost of that client
                    socketNew.close()

                    if socketNew in socket_list:
                        socket_list.remove(socketNew)




if __name__ == '__main__':

        socketConnection()
        sys.exit()


