import sys
import socket
import config_server
import select

host = config_server.host
port = config_server.port

socket_list = []

server = socket.socket(socket.AF_INET , socket.SOCK_STREAM)        # creating a socket
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)      # to close the port

def connectSocket():
    try:
        server.connect((host, port))  # Connecting sockets
        print("Successfully connected to the sever ")

    # If socket is not connected then exception is shown as error
    except socket.error as msg:
        print(("Unable to connect !") + str(msg))
        sys.exit()


def socketConnection():

    connectSocket()

    print("Start typing to connect to other clients.....")


    while True :

         socket_list = [sys.stdin , server]     # stdin is used for all interactive input

         read,write,err = select.select(socket_list, [], [])

         for socket_ in read :

             if socket_ == server:     # for incoming msgs from the server

                 Server_msg = socket_.recv(config_server.bytes)

                 if not Server_msg :

                     print("Removed from chat box ")
                     sys.exit()

                 else :

                     sys.stdout.write(Server_msg)                  # sys.stdout.write takes a string then pass to sys.out.encoding
                                                                   # to encode it and writes the resulting bytes() object to sys.stdout.buffer

                     sys.stdout.write('\nYOU :');        # print() is basically a wrapper around sys.stdout

                     sys.stdout.flush()                  # pushes out all the data

             else :

                 Reply = sys.stdin.readline()          # reads the whole line
                 server.send(Reply)                        # sends to data in bytes form to the server
                 sys.stdout.write('\nYOU :');
                 sys.stdout.flush()



if __name__ == "__main__":
    socketConnection()
    sys.exit()









